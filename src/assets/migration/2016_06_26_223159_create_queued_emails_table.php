<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueuedEmailsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('queued_emails', function (Blueprint $table) {
			$table->increments('id');
			/**
			 * All recipients are held in serialized arrays 
			 * because there may be numerous recipients
			 */
			$table->text('to');
			$table->text('cc')
				->nullable();
			$table->text('bcc')
				->nullable();
			// From stores a serialized array
			$table->text('from');
			// Reply to stores a serialized array
			$table->text('reply_to')
				->nullable();
			// File attachment paths are stored in a serialized array
			$table->text('file_attachments')
				->nullable();
			// String attachments are stored in a serialized array
			$table->longText('string_attachments')
				->nullable();
			$table->enum('mailtype', ['text', 'html'])
				->default('text');
			$table->enum('protocol', ['mail', 'smtp'])
				->default('mail');
			$table->string('smtp_user')
				->nullable();
			$table->text('smtp_pass')
				->nullable()
				->comment('Password stored encypted.');
			$table->string('smtp_host')
				->nullable();
			$table->integer('smtp_port')
				->unsigned()
				->nullable();
			$table->string('subject')
				->nullable()
				->comment('Only nullable because we may send text messages.');
			$table->text('body');
			$table->text('alt_body')
				->nullable();
			$table->dateTime('queued_at');
			$table->enum('lock', ['0', '1'])
				->default('0');
			$table->dateTime('sent_at')
				->nullable();
			$table->text('errors')
				->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('queued_emails');
	}
}
