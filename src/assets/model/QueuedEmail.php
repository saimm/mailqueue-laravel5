<?php

namespace App;

use Crypt;
use Illuminate\Database\Eloquent\Model;

class QueuedEmail extends Model
{
	public $timestamps = false;

	protected $fillable = [];

	/**
	 * Accessor for "to"
	 */
	public function getToAttribute($x){ 
		return unserialize( $x ); 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "cc"
	 */
	public function getCcAttribute($x){ 
		return ( ! empty( $x ) )
			? unserialize( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "bcc"
	 */
	public function getBccAttribute($x){ 
		return ( ! empty( $x ) )
			? unserialize( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "from"
	 */
	public function getFromAttribute($x){ 
		return unserialize( $x ); 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "reply_to"
	 */
	public function getReplyToAttribute($x){ 
		return ( ! empty( $x ) )
			? unserialize( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "file_attachments"
	 */
	public function getFileAttachmentsAttribute($x){ 
		return ( ! empty( $x ) )
			? unserialize( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "string_attachments"
	 */
	public function getStringAttachmentsAttribute($x){ 
		return ( ! empty( $x ) )
			? unserialize( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "smtp_pass"
	 */
	public function getSmtpPassAttribute($x){ 
		return ( ! is_null( $x ) )
			? Crypt::decrypt( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Accessor for "errors"
	 */
	public function getErrorsAttribute($x){ 
		return ( ! empty( $x ) )
			? unserialize( $x )
			: NULL;
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "to"
	 */
	public function setToAttribute($x){ 
		$this->attributes['to'] = serialize( $x ); 
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "cc"
	 */
	public function setCcAttribute($x){ 
		$this->attributes['cc'] = ( ! empty( $x ) )
			? serialize( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "bcc"
	 */
	public function setBccAttribute($x){ 
		$this->attributes['bcc'] = ( ! empty( $x ) )
			? serialize( $x )
			: NULL;
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "from"
	 */
	public function setFromAttribute($x){ 
		$this->attributes['from'] = serialize( $x ); 
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "reply_to"
	 */
	public function setReplyToAttribute($x){ 
		$this->attributes['reply_to'] = ( ! empty( $x ) )
			? serialize( $x )
			: NULL;
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "file_attachments"
	 */
	public function setFileAttachmentsAttribute($x){  
		$this->attributes['file_attachments'] = ( ! empty( $x ) )
			? serialize( $x )
			: NULL;
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "string_attachments"
	 */
	public function setStringAttachmentsAttribute($x){  
		$this->attributes['string_attachments'] = ( ! empty( $x ) )
			? serialize( $x )
			: NULL;
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "smtp_pass"
	 */
	public function setSmtpPassAttribute($x){ 
		$this->attributes['smtp_pass'] = ( ! is_null( $x ) )
			? Crypt::encrypt( $x )
			: NULL; 
	}
	// -----------------------------------------------------------------------

	/**
	 * Mutator for "errors"
	 */
	public function setErrorsAttribute($x){  
		$this->attributes['errors'] = ( ! empty( $x ) )
			? serialize( $x )
			: NULL;
	}
	// -----------------------------------------------------------------------
}
