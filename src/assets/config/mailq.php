<?php

/**
 * Mail Queue Config
 */
$cfg['env_defaults'] = [
	'all' => [
		'protocol'   => 'smtp',
		'mailtype'   => 'html',
	],
	'local' => [
		'smtp_host' => 'localhost',
		'smtp_user' => 'skunkbad',
		'smtp_pass' => 'nada'
	],
	'production' => []
];

return $cfg;

/* End of file mailq.php */