<?php 

namespace Skunkbad\MailQueue\Facade;
 
use Illuminate\Support\Facades\Facade;
 
class Mailq extends Facade {
 
	/**
	* Get the registered name of the component.
	*
	* @return string
	*/
	protected static function getFacadeAccessor() { return 'mailq'; }
 
}