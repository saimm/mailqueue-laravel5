<?php 

namespace Skunkbad\MailQueue;

use Illuminate\Support\ServiceProvider;
use League\HTMLToMarkdown\HtmlConverter;

class MailQueueServiceProvider extends ServiceProvider {

	protected $defer = TRUE;

	/**
	 * Boot
	 */
	public function boot()
	{
		$this->publishes([
			__DIR__ . '/assets/config/mailq.php' => config_path('mailq.php')
		], 'config');

		$this->publishes([
			__DIR__ . '/assets/migration/' => database_path('migrations')
		], 'migration');

		$this->publishes([
			__DIR__ . '/assets/model/QueuedEmail.php' => app_path('QueuedEmail.php')
		], 'model');
	}

	/**
	 * Register
	 */
	public function register()
	{

		$this->app['html_converter'] = $this->app->share(function($app){
			return new HtmlConverter;
		});

		$this->app['mailq'] = $this->app->share(function($app){
			return new Lib\Queue($app['html_converter']);
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return string[]
	 */
	public function provides()
	{
		return ['mailq'];
	}

}
