<?php
/**
 * This class's purpose is send an email through Swiftmailer.
 */ 

namespace Skunkbad\MailQueue\Lib;

use Swift_SmtpTransport;
use Swift_MailTransport;
use Swift_Mailer;
use Swift_Message;
use Swift_RfcComplianceException;
use Swift_Attachment;
use Swift_TransportException;

class Email
{
	public    $errors = [];
	protected $transport;
	protected $mailer;
	protected $message;

	/**
	 * Send an email through Swiftmailer
	 */
	public function send_email( $email )
	{
		if( $this->prep( $email ) )
		{
			if( $this->set_message_and_attachments( $email ) )
				return $this->send();
		}

		return NULL;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Prepare the swiftmailer transport and mailer objects
	 */
	protected function prep( $email )
	{
		try
		{
			// Prepare transport for sending mail through SMTP
			if( $email['protocol'] == 'smtp' )
			{
				$this->transport = Swift_SmtpTransport::newInstance( $email['smtp_host'], $email['smtp_port'] )
					->setUsername( $email['smtp_user'] )
					->setPassword( $email['smtp_pass'] );
			}

			// Prepare transport for simple mail sending
			else
			{
				$this->transport = Swift_MailTransport::newInstance();
			}

			// Prepare swiftmailer mailer object
			$this->mailer = Swift_Mailer::newInstance( $this->transport );
		}
		catch( \Exception $e )
		{
			$this->errors[] = 'MAIL PREP FAILURE';
			return FALSE;
		}

		return TRUE;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Setup the message and attachments
	 */
	protected function set_message_and_attachments( $email )
	{
		// Set the body type for the message
		$body_type = $email['mailtype'] == 'html'
			? 'text/html'
			: 'text/plain';

		// Get a new message instance, and apply its attributes
		$this->message = Swift_Message::newInstance()
			->setSubject( $email['subject'] )
			->setFrom( $email['from'] )
			->setBody( $email['body'], $body_type );

		// If there is an alternative body, add it
		if( ! empty( $email['alt_body'] ) )
			$this->message->addPart( $email['alt_body'] );

		// Recipients
		if( ! $this->add_recipients( $email ) )
			return FALSE;

		// Reply To
		if( ! empty( $email['reply_to'] ) )
			$this->message->setReplyTo( $email['reply_to'] );

		// Attachments
		if( ! $this->add_attachments( $email ) )
			return FALSE;

		return TRUE;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add recipients to the email
	 */
	protected function add_recipients( $email )
	{
		try
		{
			if( ! empty( $email['to'] ) )
			{
				$this->message->setTo( $email['to'] );
			}
			else
			{
				$this->errors[] = 'NO RECIPIENT';
				return FALSE;
			}

			if( ! empty( $email['cc'] ) )
				$this->message->setCC( $email['cc'] );

			if( ! empty( $email['bcc'] ) )
				$this->message->setTo( $email['bcc'] );
		}
		catch( Swift_RfcComplianceException $e )
		{
			$this->errors[] = 'BAD EMAIL ADDRESS';
			return FALSE;
		}

		return TRUE;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add any attachments
	 */
	protected function add_attachments( $email )
	{
		$status = [
			'file_attachments'   => $this->_add_attachments( $email, 'file' ),
			'string_attachments' => $this->_add_attachments( $email, 'string' )
		];

		if( $status['file_attachments'] === FALSE OR $status['string_attachments'] === FALSE )
			return FALSE;

		return TRUE;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add file and string attachments
	 */
	protected function _add_attachments( $email, $type )
	{
		try
		{
			if( ! empty( $email[$type . '_attachments'] ) )
			{
				foreach( $email[$type . '_attachments'] as $a )
				{
					$attachment = $type == 'file'
						? Swift_Attachment::fromPath( $a['attachment'] )
						: Swift_Attachment::newInstance( base64_decode( $a['attachment'] ) );

					if( ! empty( $a['filename'] ) )
						$attachment->setFilename( $a['filename'] );

					if( ! empty( $a['mime'] ) )
						$attachment->setContentType( $a['mime'] );

					$this->message->attach( $attachment );
				}
			}
		}
		catch( \Exception $e )
		{
			$this->errors[] =  strtoupper( $type ) . ' ATTACHMENT FAILURE';
			return FALSE;
		}

		return TRUE;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Send the email
	 */
	public function send()
	{
		try
		{
			if( $result = $this->mailer->send( $this->message ) )
				return date('Y-m-d H:i:s');
		}
		catch( Swift_TransportException $STe )
		{
			$this->errors[] = 'TRANSPORT FAILURE';
		}
		catch( \Exception $e )
		{
			$this->errors[] = 'GENERAL SEND FAILURE';
		}

		return NULL;
	}
	
	// -----------------------------------------------------------------------
}