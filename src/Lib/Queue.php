<?php 
/**
 * This class's purpose is to take everything associated with 
 * the sending of an email and stick it in the database.
 *
 * It will also pull unsent email records from the database, 
 * send them through the Email class, and then mark them as sent.
 *
 * The only deviation from this purpose is if the email is 
 * expected to be sent immediately.
 */

namespace Skunkbad\MailQueue\Lib;

use App;
use Config;
use Validator;
use App\QueuedEmail;
use League\HTMLToMarkdown\HtmlConverter;

class Queue
{
	/**
	 * Default config
	 */
	protected $defaults = [
		'to'                 => [],
		'cc'                 => [],
		'bcc'                => [],
		'from'               => [],
		'reply_to'           => [],
		'file_attachments'   => [],
		'string_attachments' => [],
		'mailtype'           => 'text',
		'protocol'           => 'mail',
		'smtp_user'          => NULL,
		'smtp_pass'          => NULL,
		'smtp_host'          => NULL,
		'smtp_port'          => NULL,
		'view'               => 'email.plainText',
		'view_data'          => [],
		'subject'            => '',
		'injected_config'    => '', // optional array key in config/mailq.php
	];

	/**
	 * Compiled config
	 */
	protected $config = [];

	/**
	 * App environment
	 */
	protected $env;

	/**
	 * HTML to markdown conversion
	 */
	public $html_converter;
	public $html_converter_strips_tags = TRUE;
	public $html_converter_removes_nodes = 'style';

	// -----------------------------------------------------------------------

	/**
	 * Class Constructor
	 */
	public function __construct( HtmlConverter $html_converter )
	{
		// Initialize config with defaults
		$this->config = $this->defaults;

		// Set app environment
		$this->env = App::environment();

		// Apply environment specific config
		$this->apply_env_specific_config();

		// Set HTML converter object
		$this->html_converter = $html_converter;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Apply any environment specific configuration for sending email
	 */
	protected function apply_env_specific_config()
	{
		// Get all defaults specific to environments
		$env_config = Config::get('mailq.env_defaults');

		// Apply the defaults that are for all environments
		if( isset( $env_config['all'] ) )
		{
			foreach( $env_config['all'] as $k => $v )
				$this->config[$k] = $v;
		}

		// Apply the defaults for this specific environment
		if( isset( $env_config[ $this->env ] ) )
		{
			foreach( $env_config[ $this->env ] as $k => $v )
				$this->config[$k] = $v;
		}
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Apply any configuration that is injected via mailq config key
	 */
	protected function apply_injected_config( $params )
	{
		// Are any params injected?
		if( isset( $params['injected_config'] ) )
		{
			// Config key is always the same
			$injected_config = Config::get( $params['injected_config'] );

			if( ! is_array( $injected_config ) )
				return FALSE;

			// Merge defaults, method params, and injected config
			$this->config = array_merge( $this->config, $params, $injected_config );
		}

		// No params injected
		else
		{
			// Merge defaults, mail config file, and method params
			$this->config = array_merge( $this->config, $params );
		}
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Use custom queue to store message in database
	 */
	public function q( $params = [], $send_now = FALSE )
	{
		// Apply any injected config
		$this->apply_injected_config( $params );

		// Ensure sanity of email config
		$this->check_config_sanity();

		// Create Body
		$body = $this->create_body( $this->config );

		// Create Alternative Body
		$alt_body = $this->create_alt_body( $this->config );

		// Insert array
		$email = [
			'to'                 => $this->config['to'],
			'cc'                 => $this->config['cc'],
			'bcc'                => $this->config['bcc'],
			'from'               => $this->config['from'],
			'reply_to'           => $this->config['reply_to'],
			'file_attachments'   => $this->config['file_attachments'],
			'string_attachments' => $this->config['string_attachments'],
			'mailtype'           => $this->config['mailtype'],
			'protocol'           => $this->config['protocol'],
			'smtp_user'          => $this->config['smtp_user'],
			'smtp_pass'          => $this->config['smtp_pass'],
			'smtp_host'          => $this->config['smtp_host'],
			'smtp_port'          => $this->config['smtp_port'],
			'subject'            => $this->config['subject'],
			'body'               => $body,
			'alt_body'           => $alt_body,
			'sent_at'            => NULL,
			'errors'             => NULL
		];

		// Send
		if( $send_now )
		{
			$status = $this->send_now( $email );

			$email['sent_at'] = $status['sent_at'];

			if( $status['errors'] )
				$email['errors'] = $status['errors'];
		}

		// Store
		$queued_email = $this->store( $email );

		// Reset 
		$this->reset();

		return $queued_email;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Make sure that the config meets some minimum requirements 
	 * so we don't end up with corrupt records in our queue.
	 */
	protected function check_config_sanity()
	{
		// Check to and from email addresses
		$arr['to'] = is_array( $this->config['to'] )
			? key( $this->config['to'] )
			: $this->config['to'];

		$arr['from'] = is_array( $this->config['from'] )
			? key( $this->config['from'] )
			: $this->config['from'];

		$validator = Validator::make( $arr, [
		    'to'   => 'email|required',
		    'from' => 'email|required',
		]);

		if( $validator->fails() )
			throw new \Exception('Not valid TO and/or FROM email address');

		// If smtp protocol, make sure connection config is not missing anything
		if( $this->config['protocol'] == 'smtp' && (
				empty( $this->config['smtp_user'] ) OR
				empty( $this->config['smtp_pass'] ) OR
				empty( $this->config['smtp_host'] ) OR
				empty( $this->config['smtp_port'] )
		))
			throw new \Exception('SMTP not configured correctly');

		// Make sure the blade template actually exists
		if( ! view()->exists( $this->config['view'] ) )
			throw new \Exception('Email view does not exist');

	}
	
	// -----------------------------------------------------------------------

	/**
	 * Create the body of the email
	 */
	protected function create_body( $config )
	{
		// View data
		$view_data = array_merge( $config, $config['view_data'] );

		// Set unique content for injection into layout
		return view( $config['view'], $view_data )->render();
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Create the alternate text version of the email if mailtype is html.
	 * If you don't want this automatic creation of a text version, simply
	 * set the html_converter property to null.
	 */
	protected function create_alt_body( $config )
	{
		if( ! is_null( $this->html_converter ) && $this->config['mailtype'] == 'html' )
		{
			// View data
			$view_data = array_merge( 
				$config, 
				$config['view_data'], 
				['text_version' => TRUE] 
			);

			// Set unique content for injection into layout
			$alt_body = view( $config['view'], $view_data )->render();

			// Strip tags removes all tags but leaves the content
			if( $this->html_converter_strips_tags )
			{
				$this->html_converter->getConfig()
					->setOption('strip_tags', TRUE);
			}

			// Remove nodes strips specified tags and removes the content
			if( ! empty( $this->html_converter_removes_nodes ) )
			{
				$this->html_converter->getConfig()
					->setOption('remove_nodes', $this->html_converter_removes_nodes );
			}

			// Return markdown
			return $this->html_converter->convert( $alt_body );
		}

		return NULL;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Send the email with Swiftmailer
	 */
	protected function send_now( $email )
	{
		$e = new Email;
		$sent_at = $e->send_email( $email );

		$errors = ( ! empty( $e->errors ) )
			? $e->errors
			: NULL;

		return [
			'sent_at' => $sent_at,
			'errors'  => $errors
		];
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Save the email in the queue
	 */
	protected function store( $email )
	{
		$queued_email = new QueuedEmail;

		$queued_email->queued_at = date('Y-m-d H:i:s');

		foreach( $email as $k => $v )
			$queued_email->{$k} = $v;

		$queued_email->save();

		return $queued_email;
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Prepare for the next email
	 */
	public function reset()
	{
		$this->config = $this->defaults;

		$this->apply_env_specific_config();
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add an attachment to the email, letting method decide
	 * if attachment is from a path or a string
	 *
	 * @param  string  a filename or a non encoded string
	 */
	public function attach( $attachment, $filename = '', $mime = '' )
	{
		// Is this a file or string?
		$method = ( strlen( $attachment ) < 256 && is_file( $attachment ) ) 
			? 'file_attach'
			: 'string_attach';

		$this->$method( $attachment, $filename, $mime );
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add an attachment to the email by file path
	 */
	public function file_attach( $attachment, $filename = '', $mime = '' )
	{
		$this->config['file_attachments'][] = [
			'attachment' => $attachment,
			'filename'   => $filename,
			'mime'       => $mime 
		];
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add an attachment to the email by string
	 */
	public function string_attach( $attachment, $filename, $mime = '' )
	{
		$this->config['string_attachments'][] = [
			'attachment' => base64_encode( $attachment ),
			'filename'   => $filename,
			'mime'       => $mime 
		];
	}
	
	// -----------------------------------------------------------------------

	/**
	 * Add a file attachment as a string attachment.
	 *
	 * This is nice to use if the file will not be 
	 * available when the queue is actually processed.
	 */
	public function attach_as_string( $attachment, $filename = '', $mime = '' )
	{
		$filename = ! empty( $filename )
			? $filename 
			: basename( $attachment );

		$attachment = file_get_contents( $attachment );

		$this->string_attach( $attachment, $filename, $mime );
	}
	
	// -----------------------------------------------------------------------

}