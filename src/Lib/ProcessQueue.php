<?php 
/**
 * The purpose of this class is to retrieve unsent queued email
 * from the database, send it, then mark it as sent.
 */

namespace Skunkbad\MailQueue\Lib;

use App\QueuedEmail;

class ProcessQueue
{
	protected $limit = 60;

	/**
	 * Class Constructor
	 */
	public function __construct()
	{
		// Get all unsent emails
		$emails = QueuedEmail::whereNull('sent_at')
			->whereNull('errors')
			->where('lock', '0')
			->take( $this->limit )
			->get();

		// Lock these emails so there is no process overlapping	
		foreach( $emails as $email )
			$ids_to_lock[] = $email->id;

		if( isset( $ids_to_lock ) )
			QueuedEmail::whereIn('id', $ids_to_lock)->update(['lock' => '1']);

		// Send each email
		foreach( $emails as $email )
		{
			$temp = $email->toArray();

			$e = new Email;
			
			// Success returns datetime (but not used for update of sent_at)
			if( $sent_at = $e->send_email( $temp ) )
				$sent_emails[] = $temp['id'];

			// Send attempts with errors are updated individually
			if( ! empty( $e->errors ) )
			{
				$email->errors = $e->errors;
				$email->save();
			}
		}

		// Update queue records for emails sent successfully
		if( isset( $sent_emails ) )
			QueuedEmail::whereIn('id', $sent_emails)->update(['sent_at' => date('Y-m-d H:i:s')]);
	}
	
	// -----------------------------------------------------------------------
}
