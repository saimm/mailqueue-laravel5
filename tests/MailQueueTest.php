<?php

use Skunkbad\MailQueue\Facade\Mailq;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MailQueueTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function fails_when_there_is_no_valid_to_address()
    {
        $config = $this->get_perfect_config();

        $config['to'] = '';

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function fails_when_there_is_no_valid_from_address()
    {
        $config = $this->get_perfect_config();

        $config['from'] = '';

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function fails_when_smtp_user_missing_and_trying_to_send_email_via_smtp()
    {
        $config = $this->get_perfect_config();

        $config['smtp_user'] = '';

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function fails_when_smtp_pass_missing_and_trying_to_send_email_via_smtp()
    {
        $config = $this->get_perfect_config();

        $config['smtp_pass'] = '';

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function fails_when_smtp_host_missing_and_trying_to_send_email_via_smtp()
    {
        $config = $this->get_perfect_config();

        $config['smtp_host'] = '';

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function fails_when_smtp_port_missing_and_trying_to_send_email_via_smtp()
    {
        $config = $this->get_perfect_config();

        $config['smtp_port'] = NULL;

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function fails_when_email_view_does_not_exist()
    {
        $config = $this->get_perfect_config();

        $config['view'] = 'a.b.c.d.e';

        $this->setExpectedException('Exception');

        Mailq::q( $config );
    }

    /** @test */
    public function queued_email_object_returned_after_insertion_in_db()
    {
        $config = $this->get_perfect_config();

        $queued_email = Mailq::q( $config );

        $this->assertEquals( $queued_email->to, 'recipient@example.com' );
    }

    /** @test */
    public function queued_email_is_successfully_stored_in_db()
    {
        $config = $this->get_perfect_config();

        $queued_email = Mailq::q( $config );

        $arr = $queued_email->toArray();

        $this->seeInDatabase( 'queued_emails', [
            'id'        => $arr['id'],
            'to'        => serialize($arr['to']),
            'from'      => serialize($arr['from']),
            'protocol'  => $arr['protocol'],
            'smtp_user' => $arr['smtp_user'],
            'smtp_host' => $arr['smtp_host'],
            'smtp_port' => $arr['smtp_port']
        ]);
    }

    /**
     * Make "perfect" config
     */
    private function get_perfect_config()
    {
        return [
            'to'        => 'recipient@example.com',
            'from'      => 'sender@example.com',
            'protocol'  => 'smtp',
            'smtp_user' => 'user',
            'smtp_pass' => 'password',
            'smtp_host' => 'localhost',
            'smtp_port' => 25,
            'view'      => 'email.htmlTest'
        ];
    }

    
}