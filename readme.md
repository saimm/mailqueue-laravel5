# Mail Queue for Laravel 5.2

1) Add to root composer.json:

```
"repositories": [
	{
		"type": "git",
		"url": "git@bitbucket.org:skunkbad/mailqueue-laravel5.git"
	}
],
"require": {
	"skunkbad/mailqueue-laravel5": "@dev"
}
```

2) In config/app.php:
  
* Add to providers: 
	Skunkbad\MailQueue\MailQueueServiceProvider::class,
* Add to aliases:
	'Mailq' => Skunkbad\MailQueue\Facade\Mailq::class,

3) Run:

* php artisan vendor:publish --provider="Skunkbad\MailQueue\MailQueueServiceProvider" --tag="config"
* php artisan vendor:publish --provider="Skunkbad\MailQueue\MailQueueServiceProvider" --tag="migration"
* php artisan vendor:publish --provider="Skunkbad\MailQueue\MailQueueServiceProvider" --tag="model"
* php artisan migrate

4) Add a schedule in app/Console/Kernel.php:

```
$schedule->call(function(){
    new ProcessQueue;
})->everyMinute();
```

5) Make sure to place at top of app/Console/Kernel.php:

```
use Skunkbad\MailQueue\Lib\ProcessQueue;
```

6) Add cron job to run once a minute

```
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```

## License

This Laravel package is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).